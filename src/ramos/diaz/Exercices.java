package ramos.diaz;

import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

class Utilities{
	public static int getByConsoleNumber(Scanner inputConsole) {
		int currentNumber = 0;
		try {
			currentNumber = inputConsole.nextInt();
		} catch (Exception e) {
			System.out.println("Ocurrió un error, se ha detenido la ejecución");
		}
		return currentNumber;
	}
	
	public static double getByConsoleDoubleNumber(Scanner inputConsole) {
		double currentNumber = 0;
		try {
			currentNumber = inputConsole.nextDouble();
		} catch (Exception e) {
			System.out.println("Ocurrió un error, se ha detenido la ejecución");
		}
		return currentNumber;
	}
	
	public static void showOriginalValue(String value) {
		System.out.println("El valor original es: " + value);
	}
}

class NumberSort{
	
	private void requestNumbers(Scanner inputConsole, int[] numbers , int limit) {
		for(int i = 0; i < limit ; i++) {
			System.out.println("Ingrese el número: " + i);
			numbers[i] = Utilities.getByConsoleNumber(inputConsole);
		}
	}
	
	private void arraySort(int[] numbers) {
		Arrays.sort(numbers);
		System.out.println("Los numeros ordenados son :" +  Arrays.toString(numbers));
	}
	
	public void execute() {
		Scanner inputConsole = new Scanner(System.in);
		int[] numbers = new int[6];
		requestNumbers(inputConsole, numbers, 6);
		Utilities.showOriginalValue(Arrays.toString(numbers));
		arraySort(numbers);
	}

}

class ReverseNumber{
	
	private double requestNumber(Scanner inputConsole) {
		System.out.println("Ingrese el número: ");
		double number = Utilities.getByConsoleDoubleNumber(inputConsole);
		return number;
	}
	
	private boolean isValid(double number) {
		return (number > 0 && number < 10);
	}
	
	private void reverse(double number) {
		if(isValid(number)) {
			String reversed = new StringBuffer(String.valueOf(number)).reverse().toString();
			Utilities.showOriginalValue(String.valueOf(number));
			System.out.println("El valor al reves es :" + reversed);
		}else {
			System.out.println("El número ingresado no es valido");
		}
	}
	
	public void execute() {
		Scanner inputConsole = new Scanner(System.in);
		double number = requestNumber(inputConsole);
		reverse(number);
	}
}

public class Exercices {



	public static void main(String[] args) {
		
		int continueExecution = 1;
		int opcExercise = 0;
		Scanner inputConsole = new Scanner(System.in);
		
		while(continueExecution == 1) {
			System.out.println("Elija el ejercicio a ejecutar"
					+ "\n1.Ordenar de mayor a menor"
					+ "\n2Cifras al reves");
			opcExercise = Utilities.getByConsoleNumber(inputConsole);
			
			if(opcExercise == 1) {	
				NumberSort numberSort = new NumberSort();
				numberSort.execute();
			}else if(opcExercise == 2) {
				ReverseNumber reverseNumber = new ReverseNumber();
				reverseNumber.execute();
			}else {
				System.out.println("Operación no reconocida. ¿Desea continuar?\n1.Si");
				continueExecution = Utilities.getByConsoleNumber(inputConsole);
			}
			System.out.println("Desea continuar\n1.Si\nPresione otro numero para salir?");
			continueExecution = Utilities.getByConsoleNumber(inputConsole);
		}
		inputConsole.close();
	}

}
