package abstraction;

import java.util.Arrays;

public abstract class Person {
	public String name;
	public int birthdayYear;
	
	public Person() {
		this.name = "Sandy";
		this.birthdayYear = 1993;
	}
	
	public int calculateAge() {
		return 2021 - this.birthdayYear;
	}
	
	public String getName() {
		return this.name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
}

interface Permissions{
	public String[] getRoles();
	public boolean canDelete();
}

class Witness extends Person{
	private boolean hasHurts;
	
	public Witness() {
		this.hasHurts = false;
	}
	
	public boolean getIfHasHurts() {
		return this.hasHurts;
	}
}

class User extends Person implements Permissions{

	private String username;
	private String password;
	@Override
	public String[] getRoles() {
		return new String[]{"VENDOR", "USER"};
	}
	
	// Polimorfismo por sobrecarga
	public String[] getRoles(boolean isLogged) {
		if(isLogged)
			return new String[]{"SUPER_ADMIN"};
		else
			return new String[]{};
	}

	@Override
	public boolean canDelete() {
		return true;
	}
	
	public String getUsername(){
		return this.username;
	}
	
	public String getPassword() {
		return this.password;
	}
	
}

class ShowImplementation {
	public static void main(String[] args) {
		System.out.println("Demo");
		Witness w = new Witness();
		System.out.println("Nombre de la clase victima " + w.getName());
		System.out.println("Edad de la clase victima " + w.calculateAge());
		System.out.println("Tiene heridas " + w.getIfHasHurts());
		
		User u = new User();
		u.setName("Eduardo");
		System.out.println("Nombre de la clase user " + u.getName());
		System.out.println("Edad de la clase user " + u.calculateAge());
		System.out.println("Obtener permisos" + Arrays.toString(u.getRoles()));
		
	}
}